# pywal-colors

Archlinux i3 & eww config

## Preview

## clean
![light](/preview/ArchLabs-44.png)
<br />

## Eww-popups
![light](/preview/ArchLabs-09.png)
<br />

## Eww-popups
![light](/preview/theme2.png)
<br />

## Fonts for eww

- **Liga SFMono Nerd Font**
- **Material\-Design\-Iconic\-Font**
- **Font Awesome 5 Free**
- **feather**

## Details
- **Distro** ArchLinux ;)
- **WM** i3-gaps
- **Panel** eww
- **power Widgets and music player** eww



## You Need
- **jq**
- **curl**
- **Mpd, Mpc, Ncmpcpp**
- **Eww**
- **pywal**
- **or**
- **chameleon.py**

- **exec eww open bar**
