static const char norm_fg[] = "#b8a5a8";
static const char norm_bg[] = "#12171b";
static const char norm_border[] = "#807375";

static const char sel_fg[] = "#b8a5a8";
static const char sel_bg[] = "#7E615E";
static const char sel_border[] = "#b8a5a8";

static const char urg_fg[] = "#b8a5a8";
static const char urg_bg[] = "#704955";
static const char urg_border[] = "#704955";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
    [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border },
};
