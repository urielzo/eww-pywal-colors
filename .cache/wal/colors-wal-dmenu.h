static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#b8a5a8", "#12171b" },
	[SchemeSel] = { "#b8a5a8", "#704955" },
	[SchemeOut] = { "#b8a5a8", "#80615F" },
};
