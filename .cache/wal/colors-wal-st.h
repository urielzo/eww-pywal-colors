const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#12171b", /* black   */
  [1] = "#704955", /* red     */
  [2] = "#7E615E", /* green   */
  [3] = "#795964", /* yellow  */
  [4] = "#7F6567", /* blue    */
  [5] = "#825058", /* magenta */
  [6] = "#80615F", /* cyan    */
  [7] = "#b8a5a8", /* white   */

  /* 8 bright colors */
  [8]  = "#807375",  /* black   */
  [9]  = "#704955",  /* red     */
  [10] = "#7E615E", /* green   */
  [11] = "#795964", /* yellow  */
  [12] = "#7F6567", /* blue    */
  [13] = "#825058", /* magenta */
  [14] = "#80615F", /* cyan    */
  [15] = "#b8a5a8", /* white   */

  /* special colors */
  [256] = "#12171b", /* background */
  [257] = "#b8a5a8", /* foreground */
  [258] = "#b8a5a8",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
